import java.util.Objects;

public class Money {
    private String currency;

    public String getCurrency() {
        return currency;
    }

    public int getValue() {
        return value;
    }

    private int value;

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Money money = (Money) o;
        return value == money.value &&
                Objects.equals(currency, money.currency);
    }

    @Override
    public int hashCode() {

        return Objects.hash(currency, value);
    }
}
