public class Dog extends Animal {

    public void bark(){
        System.out.println(name + ": gav gav");
    };

    @Override
    protected void run() {
        System.out.println(name + " dog run");
    }

}
