public class Car {
    private String color;
    int numberOfDoors;

    void setColor(String color){
        this.color = color;
    }

    String getColor(){
        return color;
    }

    void startEngine() {
        // Some code to start
        System.out.println("Start engine");
    }

    void stopEngine(){
        // some code to stop
        System.out.println("Stop engine");
    }
}
